from __future__ import unicode_literals

from django.db import models
from karyawan.models import Karyawan

# Create your models here.
class Kehadiran(models.Model):
    JENIS_KEHADIRAN_CHOICES = (
        ('izin', 'Izin'),
        ('cuti', 'Cuti'),
        ('alpa', 'Tanpa Alasan'),
        ('hadir', 'Hadir'),
    )

    karyawan = models.ForeignKey(Karyawan)
    jenis_kehadiran = models.CharField(max_length=20, choices=JENIS_KEHADIRAN_CHOICES)
    waktu = models.DateField()

    def __unicode__(self):
        return self.karyawan.nama

Jangan lupa, kita harus menampilkannya di halaman admin Django. Silahkan salin kode berikut ke file kehadiran/admin.py:

from django.contrib import admin
from kehadiran.models import Kehadiran

# Register your models here.
class KehadiranAdmin (admin.ModelAdmin):
    list_display = ['karyawan', 'jenis_kehadiran', 'waktu']
    list_filter = ('jenis_kehadiran',)
    search_fields = []
    list_per_page = 25

admin.site.register(Kehadiran, KehadiranAdmin)
